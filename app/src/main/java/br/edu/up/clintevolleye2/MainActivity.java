package br.edu.up.clintevolleye2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }

  public void onClickListar(View v){

      String url = "http://ws-briatore.rhcloud.com/ws/pessoa/listar";
      StringRequest request = new StringRequest(Request.Method.GET, url,
          new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
              Log.d("VOLLEY", response);
              try {
                JSONArray jsonArray = new JSONArray(response);
                for (int i = 0; i < jsonArray.length(); i++) {
                  JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                  String id = jsonObject.getString("id");
                  String nome = jsonObject.getString("nome");
                  Log.d("VOLLEY", "ID: " + id + " NOME: " + nome);
                }

              } catch (JSONException e) {
                e.printStackTrace();
              }


            }
          },
          new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               //programar quando estiver daando erro...
              Log.d("VOLLEY", "hummmmm, algo deu errado!");
            }
          }
      );

    RequestQueue queue = Volley.newRequestQueue(this);
    queue.add(request);
    queue.start();

  }
}













